Pod::Spec.new do |s|
  s.name         = "KasaDateIntervalPicker"
  s.version      = "1.1.0"
  s.summary      = "KasaDateIntervalPicker."

  s.description  = "A single date interval picker view, KasaDateIntervalPicker."

  s.homepage     = "https://bitbucket.org/kasadev/kasadateintervalpicker"

  s.license      = "MIT"
#  s.license      = { :type => "MIT", :file => "LICENSE" }

  s.author             = { "PayPad" => "dev@paypadapp.com" }

  s.platform     = :ios, "8.0"
  s.ios.deployment_target = "8.0"

  s.source       = { :git => "https://bitbucket.org/kasadev/kasadateintervalpicker.git", :tag => "1.1.0" }

  s.source_files  = "DateIntervalPicker/DateIntervalPickerView/*"

  s.frameworks = "UIKit"

  s.requires_arc = true

  s.dependency "KasaCalendarView"

end
