//
//  DateIntervalPickerView.swift
//  DateIntervalPicker
//
//  Created by Ismail Bozkurt on 09/04/2016.
//  The MIT License (MIT)
//
//  Copyright (c) 2016 Ismail Bozkurt
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

import UIKit
import KasaCalendarView


public protocol DateIntervalPickerViewDelegate : class {
    func dateIntervalPickerView(_ dateIntervalPickerView: DateIntervalPickerView, didUpdateStartDate startDate: Date)
    func dateIntervalPickerView(_ dateIntervalPickerView: DateIntervalPickerView, didUpdateEndDate endDate: Date)
}

open class DateIntervalPickerView: UIView, GLCalendarViewDelegate {
    
    // MARK: Private Properties
    fileprivate var calendarView: GLCalendarView!
    fileprivate var currentDateRange: GLCalendarDateRange?
    fileprivate var lastSize: CGSize! = CGSize.zero
    
    // MARK: Public Properties
    open weak var delegate: DateIntervalPickerViewDelegate?
    
    /// Default is today
    fileprivate var _startDate : Date! = DateIntervalPickerUtils.beginingOfDate(Date()) {
        didSet {
            self.currentDateRange?.beginDate = _startDate
            
            if (_startDate.compare(self._endDate) == ComparisonResult.orderedDescending){
                self.setEndDate(_startDate)
            }
        }
    }
    
    /// Default one week later
    fileprivate var _endDate : Date! = GLDateUtils.date(byAddingDays: 7, to:DateIntervalPickerUtils.endOfDate(Date())) {
        didSet {
            self.currentDateRange?.endDate = _endDate
            
            if (_endDate.compare(self._startDate) == ComparisonResult.orderedAscending){
                self.setStartDate(_endDate)
            }
        }
    }
    
    /// Default 1 year ago today.
    fileprivate var _boundingStartDate: Date! = GLDateUtils.date(byAddingDays: -365, to:DateIntervalPickerUtils.beginingOfDate(Date())) {
        didSet {
            if _boundingStartDate.compare(self._boundingEndDate) == ComparisonResult.orderedDescending {
                self._boundingEndDate = GLDateUtils.date(byAddingMonths: 1, to: _boundingStartDate)
            }
            
            if self.calendarView != nil {
                self.calendarView.firstDate = _boundingStartDate
            }
        }
    }
    
    /// Default 1 year later today.
    fileprivate var _boundingEndDate: Date! = GLDateUtils.date(byAddingDays: 365, to:DateIntervalPickerUtils.endOfDate(Date())) {
        didSet {
            if _boundingEndDate.compare(self._boundingStartDate) == ComparisonResult.orderedAscending {
                self._boundingStartDate = GLDateUtils.date(byAddingMonths: -1, to: _boundingEndDate)
            }
            
            if self.calendarView != nil{
                self.calendarView.lastDate = _boundingEndDate
            }
        }
    }
    
    open var rangeBackgroundColor: UIColor! = UIColor.blue {
        didSet {
            if let _ = self.currentDateRange {
                self.currentDateRange?.backgroundColor = rangeBackgroundColor
            }
        }
    }
    
    // MARK: Custom setters
    
    open func setStartDate(_ startDate: Date) {
        self._startDate = DateIntervalPickerUtils.beginingOfDate(startDate)
    }
    
    open func startDate() -> Date {
        return self._startDate
    }
    
    open func setEndDate(_ endDate: Date) {
        self._endDate = DateIntervalPickerUtils.endOfDate(endDate)
    }
    
    open func endDate() -> Date {
        return self._endDate
    }
    
    open func setBoundingStartDate(_ startDate: Date) {
        self._boundingStartDate = DateIntervalPickerUtils.beginingOfDate(startDate)
    }
    
    open func boundingStartDate() -> Date {
        return self._boundingStartDate
    }
    
    open func setBoundingEndDate(_ endDate: Date) {
        self._boundingEndDate = DateIntervalPickerUtils.endOfDate(endDate)
    }
    
    // MARK: Life cycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
        self.reload()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
        self.reload()
    }
    
    override open func didMoveToWindow() {
        super.didMoveToWindow()
        self.reload()
        self.focusToStartDate(false)
    }
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        
        if !self.lastSize.equalTo(self.bounds.size) {
            self.lastSize = self.bounds.size
            self.reload()
            self.focusToStartDate(false)
        }
    }
    
    // MARK: Publics
    
    open func reload() {
        if let _ = self.calendarView {
            DispatchQueue.main.async { () -> Void in
                self.calendarView.reload()
            }
        }
    }
    
    open func focusToStartDate(_ animated: Bool) {
        DispatchQueue.main.async { () -> Void in
            self.calendarView.scroll(to: self._startDate, animated: animated)
        }
    }
    
    // MARK: Setup
    
    fileprivate func setup() {
        self.lastSize = self.bounds.size
        
        setupCalendarView()
        reload()
        focusToStartDate(false)
    }
    
    fileprivate func setupCalendarView() {
        calendarView = GLCalendarView()
        addSubview(calendarView)
        setupCalendarViewConstraints()
        
        calendarView.delegate = self
        calendarView.showMagnifier = true
        var cal = calendarView.calendar
        if cal != nil {
            cal!.locale = Locale(identifier:"tr-TR")
        }
        
        calendarView.firstDate = _boundingStartDate
        calendarView.lastDate = _boundingEndDate
        
        let range : GLCalendarDateRange = createRangeWithStartDate(_startDate, endDate:_endDate)
        startWorkingOnRange(range)
    }
    
    fileprivate func setupCalendarViewConstraints() {
        self.calendarView.translatesAutoresizingMaskIntoConstraints = false
        self.calendarView.addLeadingConstrait(withMargin: 0.0)
        self.calendarView.addTopConstrait(withMargin: 0.0)
        self.calendarView.addTrailingConstrait(withMargin: 0.0)
        self.calendarView.addBottomConstrait(withMargin: 0.0)
        self.calendarView.layoutIfNeeded()
    }
    
    // MARK: GLCalendarViewDelegate
    open func calenderView(_ calendarView: GLCalendarView!, canAddRangeWithBegin beginDate: Date!) -> Bool {
        let rangeDayCount = self.currentRangeDayCount()
        if rangeDayCount == NSNotFound{
            return true
        }
        else if rangeDayCount > 0 {
            calendarView.removeRange(self.currentDateRange)
            calendarView.reload()
            return true
        }
        else {
            return false
        }
    }
    
    open func calenderView(_ calendarView: GLCalendarView!, rangeToAddWithBegin beginDate: Date!) -> GLCalendarDateRange! {
        let range = self.createRangeWithStartDate(beginDate, endDate: beginDate)
        return range
    }
    
    open func calenderView(_ calendarView: GLCalendarView!, beginToEdit range: GLCalendarDateRange!) {
        print("beginToEditRange: \(range.description)")
    }
    
    open func calenderView(_ calendarView: GLCalendarView!, finishEdit range: GLCalendarDateRange!, continueEditing: Bool) {
        print("finishEditRange: \(range.description)")
    }
    
    open func calenderView(_ calendarView: GLCalendarView!, canUpdate range: GLCalendarDateRange!, toBegin beginDate: Date!, end endDate: Date!) -> Bool {
        return true
    }
    
    open func calenderView(_ calendarView: GLCalendarView!, didUpdate range: GLCalendarDateRange!, toBegin beginDate: Date!, end endDate: Date!) {
        self.notifyDelegateWithSelectedRange(range)
    }
    
    open func calenderView(_ calendarView: GLCalendarView!, didSelect date: Date!) {
        let currentRangeDayCount = self.currentRangeDayCount()
        
        let newRangeClosure: ()->() = { [unowned self] in
            calendarView.removeRange(self.currentDateRange)
            let newRange = self.createRangeWithStartDate(date, endDate: date);
            self.startWorkingOnRange(newRange)
            self.notifyDelegateWithSelectedRange(newRange)
        }
        
        let updateCurrentRangeClosure: ()->() = { [unowned self] in
            if (date.compare((self._startDate)!) == ComparisonResult.orderedAscending) {
                self.setStartDate(date)
                self.delegate?.dateIntervalPickerView(self, didUpdateStartDate: self._startDate)
            }
            else if (date.compare((self._endDate)!) == ComparisonResult.orderedDescending) {
                self.setEndDate(date)
                self.delegate?.dateIntervalPickerView(self, didUpdateEndDate: self._endDate)
            }
            
            self.calendarView.updateRange(self.currentDateRange, withBegin: self.currentDateRange?.beginDate, end: self.currentDateRange?.endDate)
            self.calendarView.begin(toEdit: self.currentDateRange)
        }
        
        if currentRangeDayCount == NSNotFound || currentRangeDayCount > 0 {
            newRangeClosure()
        }
        else {
            updateCurrentRangeClosure()
        }
        
    }
    
    // MARK: Helpers
    
    fileprivate func midDateBetweenDates(_ firstDate: Date, secondDate: Date) -> Date{
        let timeDifference: TimeInterval = secondDate.timeIntervalSinceReferenceDate - firstDate.timeIntervalSinceReferenceDate
        return Date(timeIntervalSinceReferenceDate: (timeDifference / 2) + firstDate.timeIntervalSinceReferenceDate)
    }
    
    fileprivate func notifyDelegateWithSelectedRange(_ range: GLCalendarDateRange) {
        self.setStartDate(range.beginDate)
        self.delegate?.dateIntervalPickerView(self, didUpdateStartDate: self._startDate)
        self.setEndDate(range.endDate)
        self.delegate?.dateIntervalPickerView(self, didUpdateEndDate: self._endDate)
    }
    
    fileprivate func startWorkingOnRange(_ range:GLCalendarDateRange) {
        //        dispatch_async(dispatch_get_main_queue()) { () -> Void in
        self.calendarView.ranges.removeAllObjects()
        self.calendarView.reload()
        
        self.calendarView.addRange(range)
        self.calendarView.begin(toEdit: range)
        
        self.currentDateRange = range
        //        }
    }
    
    fileprivate func createRangeWithStartDate(_ startDate: Date, endDate: Date) -> GLCalendarDateRange {
        let range: GLCalendarDateRange = GLCalendarDateRange(begin:startDate, end:endDate)
        range.backgroundColor = self.rangeBackgroundColor
        range.editable = true
        
        return range
    }
    
    fileprivate func currentRangeDayCount() -> Int {
        var dayCount:Int = NSNotFound
        
        if let currentRange = self.calendarView.ranges.firstObject as? GLCalendarDateRange {
            dayCount = DateIntervalPickerUtils.numberOfDays(currentRange.beginDate, toDate: currentRange.endDate)
        }
        
        return dayCount
    }
}

