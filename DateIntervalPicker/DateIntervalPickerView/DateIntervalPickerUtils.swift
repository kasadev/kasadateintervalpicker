//
//  DateIntervalPickerUtils.swift
//  DateIntervalPicker
//
//  Created by Ismail Bozkurt on 10/04/2016.
//  The MIT License (MIT)
//
//  Copyright (c) 2016 Ismail Bozkurt
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

import UIKit

private let dateUnits:NSCalendar.Unit =
    [.year, .month, .day, .hour, .minute, .second, .weekOfYear, .weekday, .weekdayOrdinal]

open class DateIntervalPickerUtils {

    static func numberOfDays(_ fromDate: Date, toDate: Date, inTimeZone timeZone: TimeZone? = nil) -> Int {
        var calendar = Calendar.current
        if let timeZone = timeZone {
            calendar.timeZone = timeZone
        }
        
        var fromDateTime:NSDate?
        var toDateTime:NSDate?
        
        let nsCalendar = calendar as NSCalendar
        nsCalendar.range(of:.day, start:&fromDateTime, interval:nil, for:fromDate)
        nsCalendar.range(of:.day, start:&toDateTime, interval:nil, for:toDate)
        
        let difference = nsCalendar.components(.day, from:fromDateTime as! Date, to:toDateTime as! Date, options:[])
        return difference.day!
    }
    
    static func setHour(_ hour: Int!, minute: Int!, second: Int!, ofDate date: Date!) -> Date! {
        
        let calendar = Calendar(identifier:Calendar.Identifier.gregorian)
        //calendar.timeZone = TimeZone(identifier:"UTC")
        var comp = (calendar as NSCalendar).components(dateUnits, from:date)
        comp.hour = hour
        comp.minute = minute
        comp.second = second
        if let updatedDate = calendar.date(from: comp) {
            return updatedDate
        }
        
        return date
    }
    
    static func endOfDate(_ date: Date) -> Date {
        return self.setHour(23, minute: 59, second: 59, ofDate: date)
    }
    
    static func beginingOfDate(_ date: Date) -> Date {
        return self.setHour(0, minute: 0, second: 0, ofDate: date)
    }
}

internal extension UIView {
    func addConstraintsToSuperView(_ padding: CGFloat) {
        if superview != nil {
            translatesAutoresizingMaskIntoConstraints = false
            addTopConstrait(withMargin: padding)
            addBottomConstrait(withMargin: padding)
            addLeadingConstrait(withMargin: padding)
            addTrailingConstrait(withMargin: padding)
        }
    }
    
    
    func addConstraintAttribute(_ attribute:NSLayoutAttribute, multiplier:CGFloat=1, constant:CGFloat) {
        let constraint = NSLayoutConstraint(item:self, attribute:attribute, relatedBy:.equal, toItem:superview , attribute:attribute, multiplier:multiplier, constant:constant)
        superview?.addConstraint(constraint)
    }
    
    func addLeadingConstrait(withMargin margin:CGFloat) {
        addConstraintAttribute(.leading, constant:margin)
    }
    
    func addTrailingConstrait(withMargin margin:CGFloat) {
        addConstraintAttribute(.trailing, constant:margin)
    }
    
    func addTopConstrait(withMargin margin:CGFloat) {
        addConstraintAttribute(.top, constant:margin)
    }
    
    func addBottomConstrait(withMargin margin:CGFloat) {
        addConstraintAttribute(.bottom, constant:margin)
    }
    
    func addHeightConstrait(_ height:CGFloat) {
        addConstraintAttribute(.height, constant:height)
    }
    
}
